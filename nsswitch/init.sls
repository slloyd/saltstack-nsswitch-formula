{% from "nsswitch/map.jinja" import os_defaults with context %}

/etc/nsswitch.conf:
  file.managed:
    - user: root
    - source: salt://nsswitch/files/nsswitch.conf.jinja
    - group: {{ os_defaults.get('root_group') }}
    - mode: 444
    - template: jinja
